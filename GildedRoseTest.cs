﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        // constructor
        [Test]
        public void GildedRoseConstructorTest()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 }, 
                                        new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(2, app.Items.Count);
        }

        // aged bri increases quality the older it gets
        [Test]
        public void UpdateQualityTest_AgedBrie_sold()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = -1, Quality = 10 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            Assert.AreEqual(12, app.Items[0].Quality);
            Assert.AreEqual(-2, app.Items[0].SellIn);
        }
        

        // sulfuras doesnt change 
        [Test]
        public void UpdateQualityTest_SulfurasHandofRagnaros_neverchanges()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 10, Quality = 10 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            Assert.AreEqual(10, app.Items[0].Quality);
            Assert.AreEqual(10, app.Items[0].SellIn);
        }

        // backstage passes increase quality depending on sell approach
        [Test]
        public void UpdateQualityTest_backstagePasses_sellin_lessthan10days()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 9, Quality = 4 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            Assert.AreEqual(6, app.Items[0].Quality);
            Assert.AreEqual(8, app.Items[0].SellIn);
        }
        [Test]
        public void UpdateQualityTest_backstagePasses_sellin_lessthan5days()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 4, Quality = 4 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            Assert.AreEqual(7, app.Items[0].Quality);
            Assert.AreEqual(3, app.Items[0].SellIn);
        }

        // backstage passes drops quality to 0 after concert
        [Test]
        public void UpdateQualityTest_backstagePasses_sold()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = -1, Quality = 4 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(-2, app.Items[0].SellIn);
        }

        // when sellin is negative, quality decreases twice as much
        [Test]
        public void UpdateQualityTest_sellin_normal()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 20, Quality = 20 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // -1 for quality
            Assert.AreEqual(19, app.Items[0].Quality);
            Assert.AreEqual(19, app.Items[0].SellIn);
        }
        [Test]
        public void UpdateQualityTest_sellin_negative()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = -3, Quality = 20 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // twice as much when sellin is negative
            Assert.AreEqual(18, app.Items[0].Quality);
            Assert.AreEqual(-4, app.Items[0].SellIn);
        }

        // quality never goes less than 0
        [Test]
        public void UpdateQualityTest_quanitity_normal_decreases()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 1, Quality = 1 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // -1 for quantity
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(0, app.Items[0].SellIn);
        }
        [Test]
        public void UpdateQualityTest_quanitity_0_doesnt_change()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 1, Quality = 0 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // doesnt change because quantity is 0 already
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(0, app.Items[0].SellIn);
        }

        // quality never goes more than 50 - aged brie increases over time
        [Test]
        public void UpdateQualityTest_quanitity_normal_increases()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 1, Quality = 49 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // -1 for quantity
            Assert.AreEqual(50, app.Items[0].Quality);
            Assert.AreEqual(0, app.Items[0].SellIn);
        }
        [Test]
        public void UpdateQualityTest_quanitity_50_doesnt_change()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 1, Quality = 50 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // doesnt change because quantity is 0 already
            Assert.AreEqual(50, app.Items[0].Quality);
            Assert.AreEqual(0, app.Items[0].SellIn);
        }

        // never negative number in quntity
        [Test]
        public void UpdateQualityTest_foo_sold_1()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = -1, Quality = 1 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // doesnt change because quantity is 0 already
            Assert.AreEqual(0, app.Items[0].Quality);
            Assert.AreEqual(-2, app.Items[0].SellIn);
        }


        // when sellin is negative, quality decreases twice as much - conjured so 4 times as fast
        [Test]
        public void UpdateQualityTest_sellin_normal_conjured()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured", SellIn = 20, Quality = 20 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // -1 for quality
            Assert.AreEqual(18, app.Items[0].Quality);
            Assert.AreEqual(19, app.Items[0].SellIn);
        }
        [Test]
        public void UpdateQualityTest_sellin_negative_conjured()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured", SellIn = -3, Quality = 20 }};
            GildedRose app = new GildedRose(Items);

            app.UpdateQuality();
            // twice as much when sellin is negative
            Assert.AreEqual(16, app.Items[0].Quality);
            Assert.AreEqual(-4, app.Items[0].SellIn);
        }
    }
}
