﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        public IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                if(!(Items[i].Name == "Sulfuras, Hand of Ragnaros")){
                    // update sellin 
                    Items[i].SellIn = Items[i].SellIn - 1; 
                    switch (Items[i].Name)
                    {
                        case "Aged Brie":
                            UpdateAgedBrie(Items[i]);
                            break;
                        case "Backstage passes to a TAFKAL80ETC concert":
                            UpdateBackstagePass(Items[i]);
                            break;
                        default:
                            UpdateNormalItem(Items[i]);
                            break;
                    }
                }
            }
        }

        private void UpdateAgedBrie(Item item)
        {
            if (item.Quality < 50)
            {
                item.Quality++;
                
            }
            if (item.SellIn < 0 && item.Quality < 50)
            {
                item.Quality++;
            }
        }

        private void UpdateBackstagePass(Item item)
        {
            if (item.SellIn < 0)
            {
                item.Quality = item.Quality - item.Quality;
            }
            else if (item.Quality < 50)
            {
                item.Quality++;
                if (item.SellIn < 10 && item.Quality < 50)
                {
                    item.Quality++;
                }
                if (item.SellIn < 5 && item.Quality < 50)
                {
                    item.Quality++;
                }
            }
        }

        private void UpdateNormalItem(Item item)
        {
            if (item.Quality > 0)
            {
                item.Quality--;
                if (item.SellIn < 0 && item.Quality > 0)
                {
                    item.Quality--;
                }
            }
            if(item.Name == "Conjured"){
                item.Quality--;
                if (item.SellIn < 0 && item.Quality > 0)
                {
                    item.Quality--;
                }
            }
        }
    }
}
