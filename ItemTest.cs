using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class ItemTest
    {
        [Test]
        public void CreateItemTest()
        {
            Item item = new Item { Name = "foo", SellIn = 3, Quality = 10 };
            
            Assert.AreEqual("foo", item.Name);
            Assert.AreEqual(3, item.SellIn);
            Assert.AreEqual(10, item.Quality);
        }
        [Test]
        public void ToStringItemTest()
        {
            Item item = new Item { Name = "foo", SellIn = 3, Quality = 10 };
            
            Assert.AreEqual("foo, 3, 10", item.ToString());
        }
    }
}
